/*
*/
/**
 * Fetches JS templates via AJAX and caches them
 */
Drupal.theme.jTemplate = function(func) {
  for (var i = 1, args = []; i < arguments.length; i++) {
    args.push(arguments[i]);
  }

  Drupal.settings.jTemplates = Drupal.settings.jTemplates || {};
  Drupal.settings.jTemplates[func] = Drupal.settings.jTemplates[func] || Drupal.parseJson(jQuery.ajax({
  	url: Drupal.settings.basePath + 'jtemplate/callback/' + func,
  	async: false
  }).responseText);

  if ( args.length > 0 ) { /* ID/Class was provided that means we can set template on it */
    var ElemId = args.shift();
    var t = Drupal.settings.jTemplates[func];
    try {
      $(ElemId).setTemplate(t['template'],t['includes'],t['settings']);
      $().processTemplate.apply($(ElemId),args);
    }
    catch (e) {
      /* do something with the error */
    }
  }
}

